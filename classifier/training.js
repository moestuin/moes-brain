const fs = require('fs');
const BrainJSClassifier = require('natural-brain');
const classifier = new BrainJSClassifier();

const weatherApp = 'moon';
const gardenApp = 'moes';

const trainingsDataResource = `${__dirname}/trainingsData.txt`;
const classifierResultResource = `${__dirname}/classifier.json`;

const trainingDataBase = [
    {text: 'what is the weather like today', label: `${weatherApp}.today`},
    {text: 'how is the weather looking', label: `${weatherApp}.today`},
    {text: 'will it be sunny this weekend', label: `${weatherApp}.weekend`},
    {text: 'what is the weather like this weekend', label: `${weatherApp}.weekend`},
    {text: 'do I need to water the plants', label: `${gardenApp}.water`},
    {text: 'water?', label: `${gardenApp}.water`},
    {text: 'ask moes water?', label: `${gardenApp}.water`},
    {text: 'do the plants need water', label: `${gardenApp}.water`},
    {text: 'how are the plants doing', label: `${gardenApp}.info`},
    {text: 'how is my garden', label: `${gardenApp}.info`},
    {text: 'rain or sunshine?', label: `${weatherApp}.today`}
];

const testDataBase = [
    {text: 'does Moes need water', label: `${gardenApp}.water`},
    {text: 'what is the water level', label: `${gardenApp}.water`},
    {text: 'ask if Moes needs water', label: `${gardenApp}.water`},
    {text: 'shall I give the plants some water', label: `${gardenApp}.water`},
    {text: 'is the garden happy', label: `${gardenApp}.happy`},
    {text: 'rain or sunshine', label: `${weatherApp}.today`},
    {text: 'is it going to rain in the weekend', label: `${weatherApp}.weekend`},
];

/**
 * Train the classifier
 * @param fileSystem
 */
const train = (fileSystem = fs) => {
    fileSystem.readFile(trainingsDataResource, (err, data) => {
        const trainingData = transformData(data);

        startTraining([...trainingDataBase, ...trainingData]);

        saveClassifier(classifier);
    });
};

/**
 * Train the classifier
 * @param utterance
 * @param app
 * @param intent
 * @param trainInit
 * @param fileSystem
 */
const addTrainingsData = ({utterance, app, intent}, trainInit = train, fileSystem = fs) => {
    fileSystem.appendFile(trainingsDataResource, `${utterance}|${app}.${intent}\n`, (err) => {
        if (err) {
            console.error(err)
        } else {
            console.log(' [x] Saved utterance to training data');
        }
    });
};

/**
 * Test the classifiers accuracy
 * @param fileSystem
 */
const test = (fileSystem = fs) => {
    fileSystem.readFile(trainingsDataResource, (err, data) => {
        const trainingData = transformData(data);

        // Split data: 60% for training and 40% for testing
        const slicePoint = Math.ceil(trainingData.length * 0.6);

        startTraining([...trainingDataBase, ...trainingData.slice(0, slicePoint)]);

        testClassifier([...testDataBase, ...trainingData.slice(slicePoint)]);
    });
};

/**
 * Transform data from .txt to an object
 * @param data
 * @returns {Array.<*>}
 */
const transformData = data => {
    return data
        .toString()
        .split("\n")
        .map(line => line.split('|'))
        .map(([text, label]) => ({
            text,
            label
        }))
        .filter(({text, label}) => text && label)
};

/**
 * Add data to classifier documents
 * @param trainingData
 */
const startTraining = trainingData => {

    trainingData.forEach(({text, label}) => classifier.addDocument(text, label));

    classifier.train();

};

/**
 * Test the accuracy of the classifier
 * @param testData
 */
const testClassifier = testData => {
    console.log("Testing classifier..");

    let numCorrect = 0;

    testData.forEach(({text, label}) => {
        const labelGuess = classifier.classify(text);

        if (labelGuess === label) numCorrect++;

        console.log("\n");
        console.log(text);
        console.log("Label", label);
        console.log("Label guess", labelGuess);
        console.log(classifier.getClassifications(text));
    });

    console.log("Correct %:", numCorrect/testData.length * 100);
};

/**
 * Save the classifier to be used in Brain.js
 * @param classifier
 */
const saveClassifier = classifier => {
    classifier.save(classifierResultResource, (error, classifier) => {
        if (error) {
            console.log(error);
        } else {
            console.log("Classifier saved");
        }
    });
};

/**
 * Run
 */
module.exports = {
    addTrainingsData,
    train
};