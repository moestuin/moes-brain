const assert = require('assert');
const getIntent = require('../brain');
const expect = require('chai').expect;

const ACCURACY_BARRIER = 0.7;

describe('Brain', () => {
    describe('getIntent', () => {
        it('should return an app "moes" and intent "water"', () => {
            const onMatch = ({app, intent}) => {
                expect(app).to.equal('moes');
                expect(intent).to.equal('water');
            };

            const onNoMatch = () => {
                throw Error("Mismatched callback called when there should be a match")
            };

            getIntent('do the plants need water?', ACCURACY_BARRIER, onMatch, onNoMatch);
        });

        it('should call mismatch when utterance is not recognisable"', () => {
            const utterance = 'lorem ipsum';

            const onMatch = () => {
                throw Error("Match found while utterance is clearly not recognisable")
            };

            const onNoMatch = ({matchList}) => {
                matchList.forEach((matchedLabel) => {
                    expect(matchedLabel.accuracy).to.be.lessThan(ACCURACY_BARRIER);
                });
            };

            getIntent(utterance, ACCURACY_BARRIER, onMatch, onNoMatch);
        });

        it('should order mismatches by accuracy - most accurate first"', () => {
            const utterance = 'lorem ipsum';

            const onMatch = () => {};

            const onNoMatch = ({matchList}) => {
                matchList.reduce((a, b) => {
                    if (a) expect(a.accuracy).to.be.above(b.accuracy);
                });
            };

            getIntent(utterance, ACCURACY_BARRIER, onMatch, onNoMatch);
        });
    });
});
