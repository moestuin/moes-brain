const fs = require('fs');

const getIntent = require('./brain');
const connect = require('RabAMQP');
const {addTrainingsData} = require('./classifier/training');

const APP = 'rabot';
const KEY_CHAT_USER_MESSAGE = `rabot.chat.*.user.message`;
const KEY_CHAT_USER_INTENT = `rabot.chat.*.user.intent`;
const KEY_CHAT_BOT = (username) => `rabot.chat.${username}.chatbot`;

const KEY_APP_INTENTS = (app) => `rabot.apps.intents.${app}`;
const KEY_APP_RESPONSES = `rabot.apps.responses.#`;
const KEY_APP_NOTIFICATIONS = `rabot.apps.notifications.#`;

const ACCURACY_BARRIER = 0.7;

const mockUsers = [
    {
        username: 'vincent',
        apps: [
            'moes',
            'weather'
        ]
    },
    {
        username: 'yy',
        apps: [
            'weather'
        ]
    }
];

connect({
    appName: APP,
    onConnect: ({assertQueue, sendMessage}) => {
        /**
         * USER => APP: Listen to utterances
         */
        assertQueue(
            'rabot.user.message',
            KEY_CHAT_USER_MESSAGE,
            (message) => {
                const {utterance, username} = message;

                getIntent(
                    utterance,
                    ACCURACY_BARRIER,
                    ({app, intent}) => sendMessage({
                        ...message,
                        intent
                    }, KEY_APP_INTENTS(app)),
                    ({matchList}) => sendMessage({
                        utterance: 'I did not get that.. did you mean?',
                        type: 'mismatch',
                        matchList,
                        replyTo: message
                    }, KEY_CHAT_BOT(username))
                )
            }
        );

        /**
         * USER => APP: Listen to intents from the user
         */
        assertQueue(
            'rabot.user.intent',
            KEY_CHAT_USER_INTENT,
            ({replyTo: {utterance}, username, app, intent}) => {
                // Send intent to the app
                sendMessage({
                    utterance,
                    intent,
                    username
                }, KEY_APP_INTENTS(app));

                // Save intent for training
                addTrainingsData({
                    utterance,
                    app,
                    intent
                });
            }
        );

        /**
         * APP => USER: Forward response from apps
         */
        assertQueue(
            'rabot.response',
            KEY_APP_RESPONSES,
            (message) => {
                const {replyTo: {username}} = message;
                sendMessage(message, KEY_CHAT_BOT(username))
            }
        );

        /**
         * APP => USER: Forward notifications from apps
         */
        assertQueue(
            'rabot.notifications',
            KEY_APP_NOTIFICATIONS,
            (message) => {
                const {app} = message;
                appUser(mockUsers, app).forEach(({username}) => sendMessage(message, KEY_CHAT_BOT(username)))
            }
        );
    }
});

/**
 * Return a list of users who have this app installed
 * @param users
 * @param app
 * @returns {T|*}
 */
const appUser = (users, app) => users.filter((user) => user.apps.includes(app));