# MoesBrain

Communication between user and apps (ex. Moes and Calender).

Transforms utterances into intents.

### Example conversation

m: hi moes,

v: hi Vincent

v: do I need to water the plants

m: no need, the water level is 78. (Tomorrow would be better.)

v: what is the weather? || do I need to water the plants?

m: tomorrow would be better

v: thanks

m: you’re welcome

v: bla die bla de bla

m: sorry, did not compute. did you mean any of the following:

m: weather

m: sensor

m: …

v: weather

m: the weather is …